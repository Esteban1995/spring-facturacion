package com.htc.facturacion.model;

import java.io.Serializable;

public class Product implements Serializable{

	private static final long serialVersionUID = 4L;
	
	private Integer idProduct;
	private String name;
	private Double price;
	private Integer stock;
	private String expiration_date;
	
	public Product() {
		
	}
	
	public Product(Integer idProduct, String name, Double price, Integer stock, String expiration_date) {
		this.idProduct=idProduct;
		this.name=name;
		this.price=price;
		this.stock=stock;
		this.expiration_date=expiration_date;
	}

	public Integer getIdproduct() {
		return idProduct;
	}

	public void setIdproduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getExpiration_date() {
		return expiration_date;
	}

	public void setExpirationdate(String expiration_date) {
		this.expiration_date = expiration_date;
	}

	@Override
	public String toString() {
		return  idProduct + "..." + name + ", precio:" + price + ", Stock:" + stock
				+ ", Vence el: " + expiration_date ;
	}

	

	

}
