package com.htc.facturacion.service;

import java.util.List;

import com.htc.facturacion.model.Customer;

public interface CustomerService {
	boolean insert(Customer cus);
	
	List<Customer> loadAllCustomer();
	
	void getCustomerById(long cust_id);
	
	boolean modify(Integer id, Customer cus);
	
	boolean delete(Customer cus);
}
