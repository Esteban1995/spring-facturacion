package com.htc.facturacion.ws.endpoint;

import java.util.ArrayList;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.htc.facturacion.service.ProductService;

@Endpoint
public class FacturacionEndpoint {

	public static final String NAMESPACE_URI = "http://facturacion.htc.com";

	private ProductService service;

	public FacturacionEndpoint() {

	}

	@Autowired
	public FacturacionEndpoint(ProductService service) {
		this.service = service;
		
		@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllProductsRequest")
		@ResponsePayload
		public GetAllMoviesResponse getAllMovies(@RequestPayload GetAllMoviesRequest request) {
			GetAllMoviesResponse response = new GetAllMoviesResponse();
			List<MovieType> movieTypeList = new ArrayList<MovieType>();
			List<MovieEntity> movieEntityList = service.getAllEntities();
			for (MovieEntity entity : movieEntityList) {
				MovieType movieType = new MovieType();
				BeanUtils.copyProperties(entity, movieType);
				movieTypeList.add(movieType);
			}
			response.getMovieType().addAll(movieTypeList);

			return response;

		}
	}
}
