package com.htc.facturacion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.facturacion.dao.ProductDao;
import com.htc.facturacion.model.Product;
import com.htc.facturacion.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;

	@Override
	public boolean insert(Product pro) {
		try {
			productDao.insert(pro);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public List<Product> loadAllProducts() {
		try {
			List<Product> list = productDao.loadAllProducts();
			for (Product pro : list) {
				System.out.println(pro.toString());
			}
			return list;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public void getProductsById(long pro_id) {
		try {
			Product det = productDao.findProductsById(pro_id);
			det.getPrice();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public boolean modify(Integer id, Product pro) {
		try {
			productDao.modify(id, pro);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean delete(Product pro) {
		try {
			productDao.delete(pro);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean modifyStock(Integer id, Product pro) {
		try {
			productDao.modifyStock(id, pro);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

}
