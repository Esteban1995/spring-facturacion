package com.htc.facturacion.service;

import java.util.List;

import com.htc.facturacion.model.Detail;

public interface DetailService {

	boolean insert(Detail det);
	
	List<Detail> loadAlldetails();
	
	void getDetailById(long cust_id);
	
	boolean modify(Integer id, Detail det);
	
	boolean delete(Detail det);
	
	public Detail getDetail(Integer id, Integer catntidad);
}
