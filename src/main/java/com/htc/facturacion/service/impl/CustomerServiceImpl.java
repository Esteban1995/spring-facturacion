package com.htc.facturacion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.facturacion.dao.CustomerDao;
import com.htc.facturacion.model.Customer;
import com.htc.facturacion.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDao customerDao;

	@Override
	public boolean insert(Customer cus) {
		try {
			customerDao.insert(cus);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public List<Customer> loadAllCustomer() {
		try {
			List<Customer> listCust = customerDao.loadAllCustomer();
			for (Customer cus : listCust) {
				System.out.println(cus.toString());
			}
			return listCust;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;

	}

	@Override
	public void getCustomerById(long id_customer) {
		try {
			customerDao.findCustomerById(id_customer);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public boolean modify(Integer id, Customer cus) {
		try {
			customerDao.modify(id, cus);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean delete(Customer cus) {
		try {
			customerDao.delete(cus);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

}
