package com.htc.facturacion.dao.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.htc.facturacion.dao.BillDao;
import com.htc.facturacion.model.Bill;

@Repository
public class BillDaoImpl extends JdbcDaoSupport implements BillDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(Bill bil) {
		try {
			String sql = "INSERT INTO bills "
					+ "(id_bill, creation_date, id_customer, subtotal, iva, discount, total) VALUES (?, ?, ?, ?, ?, ?, ?)";
			getJdbcTemplate().update(sql, new Object[] { bil.getIdBill(), bil.getCdate(), bil.getIdCustomer(),
					bil.getSubtotal(), bil.getIva(), bil.getDiscount(), bil.getTotal()

			});
			return Boolean.TRUE;

		} catch (Exception e) {

			System.out.println(e);
			return Boolean.FALSE;
		}
	}
		
	
	
	@Override
	public List<Bill> loadAllBill() {
		String sql = "SELECT * FROM bills";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<Bill> result = new ArrayList<Bill>();
		for (Map<String, Object> row : rows) {
			Bill obj = new Bill();
			obj.setIdBill((Integer) row.get("id_bill"));
			obj.setCdate((Date) row.get("creation_date"));
			obj.setIdCustomer((Integer) row.get("id_customer"));
			obj.setSubtotal((Double) row.get("subtotal"));
			obj.setIva((Double) row.get("iva"));
			obj.setDiscount((Double) row.get("discount"));
			obj.setTotal((Double) row.get("total"));
			result.add(obj);
		}

		return result;

	}

	@Override
	public Bill findBillById(long bill_id) {
		String sql = "SELECT * FROM bills WHERE id_bill= ?";
		return (Bill) getJdbcTemplate().queryForObject(sql, new Object[] { bill_id }, new RowMapper<Bill>() {
			@Override
			public Bill mapRow(ResultSet rs, int rwNumber) throws SQLException {
				Bill b = new Bill();
				b.setSubtotal(rs.getDouble("subtotal"));
				b.setIva(rs.getDouble("iva"));
				b.setDiscount(rs.getDouble("discount"));
				return b;
			}
		});
	}

	@Override
	public boolean modify(Integer id, Bill bil) {
		String sql = "UPDATE bills SET subtotal = ?, iva = ?, discount = ?, total = ? WHERE id_bill =" + id;
		getJdbcTemplate().update(sql,
				new Object[] { bil.getSubtotal(), bil.getIva(), bil.getDiscount(), bil.getTotal() });
		return true;
	}

	@Override
	public boolean delete(Bill bil) {
		String sql = "DELETE FROM bills WHERE id_bill = ?";
		getJdbcTemplate().update(sql, new Object[] { bil.getIdBill() });
		return true;
	}

	@Override
	public boolean modifySubTotal(Integer id, Bill bil) {
		String sql = "UPDATE bills SET subtotal = ? WHERE id_bill =" + id;
		getJdbcTemplate().update(sql, new Object[] { bil.getSubtotal() });
		return true;
	}

	@Override
	public boolean modifyID(Integer id, Bill bil) {
		String sql = "UPDATE bills SET iva = ?, discount = ?, total = ? WHERE id_bill =" + id;
		getJdbcTemplate().update(sql, new Object[] { bil.getIva(), bil.getDiscount(), bil.getTotal() });
		return true;
	}
	
	public Integer insertAndReturn(Bill bill) {
	    final String INSERT_SQL = "INSERT INTO bills " +
	         "(creation_date, id_customer, subtotal, iva, discount, total) VALUES (?, ?, ?, ?, ?, ?)" ;
	    KeyHolder keyHolder = new GeneratedKeyHolder();
	    getJdbcTemplate().update(
	        new PreparedStatementCreator() {
	        @Override
	        public PreparedStatement createPreparedStatement(java.sql.Connection arg0) throws SQLException {
	          PreparedStatement ps =
	                      arg0.prepareStatement(INSERT_SQL, new String[] {"id_bill"});  
	            ps.setDate(1, (Date) bill.getCdate());
	            ps.setInt(2, bill.getIdCustomer());
	            ps.setDouble(3, bill.getSubtotal());
	            ps.setDouble(4, bill.getIva());
	            ps.setDouble(5, bill.getDiscount());
	            ps.setDouble(6, bill.getTotal());
	                  return ps;
	        }
	            
	        },
	        keyHolder);
	    return (Integer) keyHolder.getKey(); //now contains the generated key
	  }

}
