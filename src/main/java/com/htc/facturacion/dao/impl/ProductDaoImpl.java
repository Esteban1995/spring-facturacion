package com.htc.facturacion.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.facturacion.dao.ProductDao;
import com.htc.facturacion.model.Product;

@Repository
public class ProductDaoImpl extends JdbcDaoSupport implements ProductDao {

	@Autowired
	DataSource dataSource;
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(Product pro) {
		String sql = "INSERT INTO products "
				+ "(id_product, name, price, stock, expiration_date) VALUES (?, ?, ?, ?, ?)";
		getJdbcTemplate().update(sql, new Object[] { pro.getIdproduct(), pro.getName(), pro.getPrice(), pro.getStock(),
				pro.getExpiration_date() });
		return true;
	}

	@Override
	public List<Product> loadAllProducts() {
		String sql = "SELECT * FROM products";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<Product> result = new ArrayList<Product>();
		for (Map<String, Object> row : rows) {
			Product list = new Product();
			list.setIdproduct((Integer) row.get("id_product"));
			list.setName((String) row.get("name"));
			list.setPrice((Double) row.get("price"));
			list.setStock((Integer) row.get("stock"));
			list.setExpirationdate((String) row.get("expiration_date"));
			result.add(list);
		}

		return result;

	}

	@Override
	public Product findProductsById(long pro_id) {
		String sql = "SELECT * FROM products WHERE id_product= ?";
		return (Product) getJdbcTemplate().queryForObject(sql, new Object[] { pro_id }, new RowMapper<Product>() {
			@Override
			public Product mapRow(ResultSet rs, int rwNumber) throws SQLException {
				Product pro_1 = new Product();
				pro_1.setIdproduct(rs.getInt("id_product"));
				pro_1.setName(rs.getString("name"));
				pro_1.setPrice(rs.getDouble("price"));
				pro_1.setStock(rs.getInt("stock"));
				pro_1.setExpirationdate(rs.getString("expiration_date"));
				return pro_1;
			}
		});
	}

	@Override
	public boolean modify(Integer id, Product pro) {
		String sql = "UPDATE products SET name = ?, price = ?, stock = ?, expiration_date = ? WHERE id_product =" + id;
		getJdbcTemplate().update(sql,
				new Object[] { pro.getName(), pro.getPrice(), pro.getStock(), pro.getExpiration_date() });
		return true;
	}

	@Override
	public boolean delete(Product pro) {
		String sql = "DELETE FROM products WHERE id_product = ?";
		getJdbcTemplate().update(sql, new Object[] { pro.getIdproduct() });
		return true;
	}

	@Override
	public boolean modifyStock(Integer id, Product pro) {
		String sql = "UPDATE products SET stock = ? WHERE id_product =" + id;
		getJdbcTemplate().update(sql, new Object[] { pro.getStock() });
		return true;
	}

	@Override
	public void updateBatch(List<Product> products) {
		String sql = "UPDATE products SET stock = ? WHERE id_product = ?";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				
				Product pro = products.get(i);
				ps.setInt(1, pro.getStock());
				ps.setInt(2, pro.getIdproduct());
				
			}
			
			public int getBatchSize() {
				return products.size();
			}
		});
		
	}

}

