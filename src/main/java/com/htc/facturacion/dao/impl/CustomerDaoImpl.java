package com.htc.facturacion.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.facturacion.dao.CustomerDao;
import com.htc.facturacion.model.Customer;

@Repository
public class CustomerDaoImpl extends JdbcDaoSupport implements CustomerDao {

	@Autowired
	DataSource dataSource;
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(Customer cus) {

		String sql = "INSERT INTO customers " + "(id_customer, name, lastname, direccion) VALUES (?, ?, ?, ?)";
		getJdbcTemplate().update(sql,
				new Object[] { cus.getIdCustomer(), cus.getName(), cus.getLastname(), cus.getDireccion() });
		return true;

	}

	@Override
	public List<Customer> loadAllCustomer() {
		String sql = "SELECT * FROM customers";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<Customer> result = new ArrayList<Customer>();
		for (Map<String, Object> row : rows) {
			Customer cus = new Customer();
			cus.setIdCustomer((Integer) row.get("id_customer"));
			cus.setName((String) row.get("name"));
			cus.setLastname((String) row.get("lastname"));
			cus.setDireccion((String) row.get("direccion"));
			result.add(cus);
		}

		return result;

	}

	@Override
	public Customer findCustomerById(long id_customer) {
		String sql = "SELECT * FROM customers WHERE id_customer = ?";
		return (Customer) getJdbcTemplate().queryForObject(sql, new Object[] { id_customer },
				new RowMapper<Customer>() {
					@Override
					public Customer mapRow(ResultSet rs, int rwNumber) throws SQLException {
						Customer cust = new Customer();
						cust.setIdCustomer(rs.getInt("id_customer"));
						cust.setName(rs.getString("name"));
						cust.setLastname(rs.getString("lastname"));
						cust.setDireccion(rs.getString("direccion"));
						return cust;
					}
				});
	}

	@Override
	public boolean modify(Integer id, Customer cus) {
		String sql = "UPDATE customers SET name = ?,  lastname = ?, direccion = ? WHERE id_customer =" + id;
		getJdbcTemplate().update(sql, new Object[] { cus.getName(), cus.getLastname(), cus.getDireccion() });
		return true;
	}

	@Override
	public boolean delete(Customer cus) {
		String sql = "DELETE FROM customers WHERE id_customer = ?";
		getJdbcTemplate().update(sql, new Object[] { cus.getIdCustomer() });
		return true;
	}

}
