package com.htc.facturacion.model;

import java.io.Serializable;

public class Detail implements Serializable {

	private static final long serialVersionUID = 3L;

	private Integer idDetail;
	private Integer idBill;
	private Integer idProduct;
	private Integer quantity;
	private Double subtotal;
	
	public Detail() {

	}

	public Detail(Integer idDetail, Integer idBill, Integer idProduct, Integer quantity, Double subtotal) {
		this.idDetail = idDetail;
		this.idBill = idBill;
		this.idProduct = idProduct;
		this.quantity = quantity;
		this.subtotal = subtotal;
		
	}

	public Integer getIddetail() {
		return idDetail;
	}

	public void setIddetail(Integer idDetail) {
		this.idDetail = idDetail;
	}

	public Integer getIdbill() {
		return idBill;
	}

	public void setIdbill(Integer idBill) {
		this.idBill = idBill;
	}

	public Integer getIdProduct() {
		return idProduct;
	}

	public void setIdproduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}


	@Override
	public String toString() {
		return "detail [id_detail=" + idDetail + ", id_bill=" + idBill + ", id_product=" + idProduct + ", quantity="
				+ quantity + ", subtotal=" + subtotal + "]";
	}

}
