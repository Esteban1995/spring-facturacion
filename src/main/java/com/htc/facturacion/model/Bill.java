package com.htc.facturacion.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Bill implements Serializable{

	private static final long serialVersionUID = 2L;
	
	private Integer idBill;
	private Date cDate;
	private Integer idCustomer;
	private Double subtotal;
	private Double iva;
	private Double discount;
	private Double total;
	private List<Detail> detail;
	
	public Bill() {
		
	}
	
	public Bill(Integer idBill, Date cDate, Integer idCustomer, Double subtotal, Double iva, Double discount, Double total, List<Detail> detail) {
		this.idBill=idBill;
		this.cDate=cDate;
		this.idCustomer=idCustomer;
		this.subtotal=subtotal;
		this.iva=iva;
		this.discount=discount;
		this.total=total;
		this.detail=detail;
	}

	public Integer getIdBill() {
		return idBill;
	}

	public void setIdBill(Integer idBill) {
		this.idBill = idBill;
	}

	public Date getCdate() {
		return cDate;
	}

	public void setCdate(Date cDate) {
		this.cDate = cDate;
	}

	public Integer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Integer idCustomer) {
		this.idCustomer = idCustomer;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Double getIva() {
		return iva;
	}

	public void setIva(Double iva) {
		this.iva = iva;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
	
	public List<Detail> getDetail() {
		return detail;
	}

	public void setDetail(List<Detail> detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return "bill [idbill=" + idBill + ", cdate=" + cDate + ", idcustomer=" + idCustomer + ", subtotal="
				+ subtotal + ", iva=" + iva + ", discount=" + discount + ", total=" + total + "]";
	}
	
	

	
}
