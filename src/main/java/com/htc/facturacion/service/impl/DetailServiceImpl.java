package com.htc.facturacion.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.facturacion.dao.DetailDao;
import com.htc.facturacion.model.Detail;
import com.htc.facturacion.service.DetailService;

@Service
public class DetailServiceImpl implements DetailService {

	@Autowired
	DetailDao detailDao;

	@Override
	public boolean insert(Detail det) {
		detailDao.insert(det);
		return true;
	}

	@Override
	public List<Detail> loadAlldetails() {
		try {
			List<Detail> list = detailDao.loadAlldetail();
			for (Detail cus : list) {
				System.out.println(cus.toString());
			}
			return list;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public boolean modify(Integer id, Detail det) {
		try {
			detailDao.modify(id, det);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean delete(Detail det) {
		try {
			detailDao.delete(det);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public void getDetailById(long cust_id) {
		try {
			detailDao.finddetailById(cust_id);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public Detail getDetail(Integer id, Integer catntidad) {
		Detail det = new Detail();
		det.setIdproduct(id);
		det.setQuantity(catntidad);
		return det;
	}

}
