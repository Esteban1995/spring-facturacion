package com.htc.facturacion.dao;

import java.util.List;

import com.htc.facturacion.model.Detail;

public interface DetailDao {

	boolean insert(Detail det);

	List<Detail> loadAlldetail();

	Detail finddetailById(long det_id);
	
	boolean modify(Integer id, Detail det);
	
	boolean delete(Detail det);

	boolean insertBatch(List<Detail> detail, Integer llave);
}
