package com.htc.facturacion.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.facturacion.dao.DetailDao;
import com.htc.facturacion.model.Detail;

@Repository
public class DetailDaoImpl extends JdbcDaoSupport implements DetailDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(Detail det) {
		String sql = "INSERT INTO details "
				+ "(id_detail, id_bill, id_product, quantity, subtotal) VALUES (?, ?, ?, ?, ?)";
		getJdbcTemplate().update(sql, new Object[] { det.getIddetail(), det.getIdbill(), det.getIdProduct(),
				det.getQuantity(), det.getSubtotal() });
		return true;

	}

	@Override
	public List<Detail> loadAlldetail() {
		String sql = "SELECT * FROM details WHERE id_bill=";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<Detail> result = new ArrayList<Detail>();
		for (Map<String, Object> row : rows) {
			Detail list = new Detail();
			list.setIdbill((Integer) row.get("id_detail"));
			list.setIdbill((Integer) row.get("id_bill"));
			list.setIdproduct((Integer) row.get("id_product"));
			list.setQuantity((Integer) row.get("Quantity"));
			list.setSubtotal((Double) row.get("subtotal"));
			result.add(list);
		}

		return result;
	}

	@Override
	public Detail finddetailById(long det_id) {
		String sql = "SELECT * FROM details WHERE id_bill = ?";
		return (Detail) getJdbcTemplate().queryForObject(sql, new Object[] { det_id }, new RowMapper<Detail>() {
			@Override
			public Detail mapRow(ResultSet rs, int rwNumber) throws SQLException {
				Detail det1 = new Detail();
				det1.setIddetail(rs.getInt("id_detail"));
				det1.setIdbill(rs.getInt("id_bill"));
				det1.setIdproduct(rs.getInt("id_product"));
				det1.setQuantity(rs.getInt("quantity"));
				det1.setSubtotal(rs.getDouble("subtotal"));
				return det1;
			}
		});

	}

	@Override
	public boolean modify(Integer id, Detail det) {
		String sql = "UPDATE details SET id_bill = ?,  id_product = ?, quantity = ?, subtotal = ? WHERE id_detail ="
				+ id;
		getJdbcTemplate().update(sql,
				new Object[] { det.getIdbill(), det.getIdProduct(), det.getQuantity(), det.getSubtotal() });
		return true;
	}

	@Override
	public boolean delete(Detail det) {
		String sql = "DELETE FROM details WHERE id_detail = ?";
		getJdbcTemplate().update(sql, new Object[] { det.getIdbill() });
		return true;
	}
	
	
		
	@Override
	public boolean insertBatch(List<Detail> detail, Integer llave) {
		String sql = "INSERT INTO details " + "(id_bill, id_product, quantity, subtotal) VALUES (?, ?, ?, ?)";
		Boolean result = false;
		try {
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				
				Detail det = detail.get(i);
				ps.setInt(1, llave);
				ps.setInt(2, det.getIdProduct());
				ps.setInt(3, det.getQuantity());
				ps.setDouble(4, det.getSubtotal());
			}
			
			public int getBatchSize() {
				return detail.size();
			}
		});
		result = true;
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return result;
 
	}

	

}
