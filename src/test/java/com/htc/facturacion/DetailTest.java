package com.htc.facturacion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.facturacion.model.Detail;
import com.htc.facturacion.service.DetailService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DetailTest {

	@Autowired
	DetailService detService;

	@Test
	public void TestInsertBill() {
		Detail det = new Detail();
		det.setIddetail(5);
		det.setIdbill(2);
		det.setIdproduct(2);
		det.setQuantity(1);
		det.setSubtotal(2.30);

		assertEquals(true, detService.insert(det));
	}

	@Test
	public void TestModify() {
		Detail det = new Detail();
		det.setIdbill(12);
		det.setIdproduct(6);
		det.setQuantity(20);
		det.setSubtotal(250.00);

		assertEquals(true, detService.modify(12, det));
	}

	@Test
	public void TestDelete() {
		Detail det = new Detail();
		det.setIddetail(2);

		assertEquals(true, detService.delete(det));
	}

	@Test
	public void findDetail() {

		assertNotNull(detService.loadAlldetails());

	}


}
