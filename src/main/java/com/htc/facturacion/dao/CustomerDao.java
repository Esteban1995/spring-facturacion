package com.htc.facturacion.dao;

import java.util.List;

import com.htc.facturacion.model.Customer;

public interface CustomerDao {
	
	boolean insert(Customer cus);
	
	List<Customer> loadAllCustomer();
	
	Customer findCustomerById(long cust_id);
	
	boolean modify(Integer id, Customer cus);
	
	boolean delete(Customer cus);

}
