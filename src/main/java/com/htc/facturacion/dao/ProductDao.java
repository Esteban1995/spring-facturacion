package com.htc.facturacion.dao;

import java.util.List;

import com.htc.facturacion.model.Product;

public interface ProductDao {

    boolean insert(Product pro);
	
	List<Product> loadAllProducts();
	
	Product findProductsById(long pro_id);

    boolean modify(Integer id, Product pro);
    
    boolean modifyStock(Integer id, Product pro);

    boolean delete(Product pro);

	void updateBatch(List<Product> products);
	
}
