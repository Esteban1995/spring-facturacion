package com.htc.facturacion.service;

import java.util.List;

import com.htc.facturacion.model.Product;

public interface ProductService {

	boolean insert(Product pro);

	List<Product> loadAllProducts();

	void getProductsById(long pro_id);

	boolean modify(Integer id, Product pro);

	boolean modifyStock(Integer id, Product pro);

	boolean delete(Product pro);

}
