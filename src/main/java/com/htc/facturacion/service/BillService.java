package com.htc.facturacion.service;

import java.util.List;

import com.htc.facturacion.model.Bill;

public interface BillService {

	boolean insert(Bill bil);

	List<Bill> loadAllBill();

	boolean modify(Integer id, Bill bil);

	boolean delete(Bill bil);

	boolean modifySubTotal(Integer id, Bill bil);

	boolean modifyID(Integer id, Bill bil);
	
	public Integer insertAndReturn(Bill bill);

}
