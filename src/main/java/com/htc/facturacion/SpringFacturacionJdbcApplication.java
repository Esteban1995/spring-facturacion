package com.htc.facturacion;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.htc.facturacion.dao.BillDao;
import com.htc.facturacion.dao.ProductDao;
import com.htc.facturacion.service.BillService;
import com.htc.facturacion.service.CustomerService;
import com.htc.facturacion.service.DetailService;
import com.htc.facturacion.service.ProductService;

@SpringBootApplication
@ComponentScan
public class SpringFacturacionJdbcApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringFacturacionJdbcApplication.class, args);
	}

	@Autowired
	CustomerService cusService;
	@Autowired
	BillService bilService;
	@Autowired
	BillDao bilDao;
	@Autowired
	ProductService proService;
	@Autowired
	ProductDao productDao;
	@Autowired
	DetailService detailService;

	@Override
	public void run(String... args) throws Exception {
		/*Scanner lectura = new Scanner(System.in);
		String n, l, d;
		int p, o = 1, v, i, sd3, stk, stk1;
		Double pr, s, st, st1, si, iva, sd2, desc, desc1;
		java.util.Date fecha = new java.util.Date();
		java.sql.Date fechasql = new java.sql.Date(fecha.getTime());
		
		List<Detail> bil = new ArrayList<>();
		bil.add(detailService.getDetail(4, 2));
		Bill bil1 = new Bill();
		bil1.setCdate(fechasql);
		bil1.setIdCustomer(1);
		bil1.setDetail(bil);
		bilService.insert(bil1);*/
		// Creacion del cliente
	/*	System.out.println("Ingrese nombre del cliente...");
		n = lectura.nextLine();
		System.out.println("Ingrese apellido del clinte...");
		l = lectura.nextLine();
		System.out.println("Ingrese la direccion...");
		d = lectura.nextLine();
		Random r1 = new Random();
		int cr = r1.nextInt(10000);
		Customer cus_1 = new Customer();
		cus_1.setIdCustomer(cr);
		cus_1.setName(n);
		cus_1.setLastname(l);
		cus_1.setDireccion(d);
		// servicio que almacena los datos en la BD
		cusService.insert(cus_1);
		System.out.println("Cliente ingresado...");

		// Creacion de factura
		java.util.Date fecha = new java.util.Date();
		java.sql.Date fechasql = new java.sql.Date(fecha.getTime());
		Random r2 = new Random();
		int br = r2.nextInt(20000);
		Bill bill_1 = new Bill();
		bill_1.setCdate(fechasql);
		bill_1.setIdCustomer(cr);
		bill_1.setSubtotal(0.00);
		bill_1.setIva(0.00);
		bill_1.setDiscount(0.00);
		bill_1.setTotal(0.00);
		// Servicio que almacena los datos en la BD
		Integer llave = bilService.insertAndReturn(bill_1);
		System.out.println("Factura creada..." + llave);
		System.out.println("Factura creada..." + br);

		// Bucle para compras a realizar
		/*while (o < 3) {
			System.out.println("Desea agregar producto?");
			System.out.println("1.Si");
			System.out.println("2.No");
			System.out.println("3.Cancelar");
			o = Integer.parseInt(lectura.nextLine());

			// Inicio de la compra
			if (o == 1) {
				System.out.println("Seleccione el producto que desea comprar...\n");
				proService.loadAllProducts();
				p = Integer.parseInt(lectura.nextLine());
				// datos obtenidos del producto seleccionado
				Product det = productDao.findProductsById(p);
				pr = det.getPrice();
				i = det.getIdproduct();
				stk = det.getStock();

				System.out.println("Cantidad de productos a comprar");
				v = Integer.parseInt(lectura.nextLine());
				
				// validacion de las existencias del producto seleccionado
				if (v <= stk && stk > 0) {
					s = pr * v;
					stk1 = stk - v;
					Product stockc = new Product();
					stockc.setStock(stk1);
					productDao.modifyStock(p, stockc);
					// Creacion de detalle factura
					Random r3 = new Random();
					int dr = r3.nextInt(20000);
					Detail det_1 = new Detail();
					det_1.setIddetail(dr);
					det_1.setIdbill(br);
					det_1.setIdproduct(i);
					det_1.setQuantity(v);
					det_1.setSubtotal(s);
					// Servicio que almacena los datos en la BD
					detailService.insert(det_1);
					System.out.println("Producto agregado...");
					
					// Se suma el sub-total de la compra a la tabla factura
					Bill bil = bilDao.findBillById(br);
					st = bil.getSubtotal();

					st1 = st + s;
					Bill bm = new Bill();
					bm.setSubtotal(st1);
					bilService.modifySubTotal(br, bm);

				} else {
					System.out.println("No hay existencias para el producto seleccionado");
				}

			} else if (o == 2) {
				// calculos de iva, descuento y total de la compra
				Bill bil1 = bilDao.findBillById(br);
				si = bil1.getSubtotal();
				// iva
				Detail det_2 = new Detail();
				iva = si * det_2.getIva();
				// descuento
				System.out.println("Ingrese descuento");
				sd3 = Integer.parseInt(lectura.nextLine());
				desc = Double.valueOf(sd3) / 100;
				desc1 = si * desc;
				// total de la compra
				sd2 = (iva + si) - desc1;
				// se agregan los valores a la tabla factura.
				Bill bil2 = new Bill();
				bil2.setIva(iva);
				bil2.setDiscount(desc1);
				bil2.setTotal(sd2);
				bilService.modifyID(br, bil2);

				System.out.println("Compra Finalizada...");
				System.exit(0);

			}else if(o == 3) {
				
			}
		}*/

	}

}

