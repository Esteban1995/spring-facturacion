package com.htc.facturacion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.facturacion.dao.BillDao;
import com.htc.facturacion.dao.DetailDao;
import com.htc.facturacion.dao.ProductDao;
import com.htc.facturacion.model.Bill;
import com.htc.facturacion.model.Detail;
import com.htc.facturacion.model.Product;
import com.htc.facturacion.service.BillService;

@Service
public class BillServiceImpl implements BillService {

	@Autowired
	BillDao billDao;
	@Autowired
	ProductDao productDao;
	@Autowired
	DetailDao detailDao;

	@Override
	public boolean insert(Bill bill) {
		Boolean result = Boolean.FALSE;
		try {
			if (bill != null || bill.getDetail() != null || bill.getDetail().size() != 0) {
				result = true;
			}
			if (result) {
				Double subtotal = 0.00;
				List<Product> products = new ArrayList<>();
				for (Detail d : bill.getDetail()) {
					Product p = productDao.findProductsById(d.getIdProduct());
					if (p == null) {
						System.out.println("no se encontró el producto");
						return false;
					}
					if (p.getStock() >= d.getQuantity()) {
						d.setSubtotal(p.getPrice() * d.getQuantity());
						subtotal = d.getSubtotal();

						Integer stock = (p.getStock() - d.getQuantity());
						p.setStock(stock);

						productDao.modifyStock(p.getIdproduct(), p);
						products.add(p);
					} else {

						System.out.println("no hay suficiente producto");
						return false;
					}
				}

				bill.setIva(subtotal);
				bill.setSubtotal(subtotal);
				bill.setDiscount(0.00);
				bill.setTotal(bill.getIva() + bill.getSubtotal());

				Integer llave = billDao.insertAndReturn(bill);

				if (llave == null) {
					System.out.println("no se pudo insertar factura");
					return false;
				}

				productDao.updateBatch(products);
				bill.setIdBill(llave);
				result = detailDao.insertBatch(bill.getDetail(), llave);
			}else {
				System.out.println("No se puede ingresar la factura.");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());

		}
		return result;
	}

	@Override
	public boolean modify(Integer id, Bill bil) {
		try {
			billDao.modify(id, bil);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean delete(Bill bil) {
		try {
			billDao.delete(bil);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.TRUE;
	}

	@Override
	public List<Bill> loadAllBill() {
		try {
			List<Bill> list = billDao.loadAllBill();
			for (Bill cus : list) {
				System.out.println(cus.toString());
			}
			return list;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public boolean modifySubTotal(Integer id, Bill bil) {
		try {
			billDao.modifySubTotal(id, bil);
			return Boolean.FALSE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean modifyID(Integer id, Bill bil) {
		try {
			billDao.modifyID(id, bil);
			return Boolean.TRUE;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return Boolean.FALSE;
	}

	@Override
	public Integer insertAndReturn(Bill bill) {
		try {
			// billDao.insertAndReturn(bill);
			return billDao.insertAndReturn(bill);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

}
