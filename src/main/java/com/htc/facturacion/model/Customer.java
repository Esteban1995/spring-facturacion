package com.htc.facturacion.model;

import java.io.Serializable;

public class Customer implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer idCustomer;
	private String name;
	private String lastname;
	private String direccion;
	
	public Customer() {
		
	}
	
	public Customer(Integer idCustomer, String name, String lastname, String direccion) {
		this.idCustomer=idCustomer;
		this.name=name;
		this.lastname=lastname;
		this.direccion=direccion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Integer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Integer idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "customer [id_customer=" + idCustomer + ", name=" + name + ", lastname=" + lastname + ", direccion="
				+ direccion + "]";
	}
	
}
