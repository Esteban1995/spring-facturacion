package com.htc.facturacion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.facturacion.dao.CustomerDao;
import com.htc.facturacion.model.Customer;
import com.htc.facturacion.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerTest {
	@Autowired
	CustomerService cusService;
	CustomerDao cusDao;

	@Test
	public void TestInsert() {
		Customer cus = new Customer();
		cus.setIdCustomer(99);
		cus.setName("Esteban");
		cus.setLastname("Hernandez");
		cus.setDireccion("Sonsonate");

		assertEquals(true, cusService.insert(cus));
	}

//	@Test
//	public void TestModify() {
//		Customer cus = new Customer();
//		cus.setName("Gerardo");
//		cus.setLastname("Hernandez");
//		cus.setDireccion("sonsonate");
//
//		assertEquals(true, cusService.modify(1, cus));
//	}
//
//	@Test
//	public void TestDelete() {
//		Customer cus = new Customer();
//		cus.setIdCustomer(1);
//
//		assertEquals(true, cusService.delete(cus));
//	}
//
//	@Test
//	public void findCustomer() {
//
//		assertNotNull(cusService.loadAllCustomer());
//
//	}

}

