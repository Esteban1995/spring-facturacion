package com.htc.facturacion.dao;

import java.util.List;

import com.htc.facturacion.model.Bill;

public interface BillDao {
	
    boolean insert(Bill bil);
	
	List<Bill> loadAllBill();
	
	public Integer insertAndReturn(Bill bill);
	
	Bill findBillById(long bill_id);

	boolean modify(Integer id, Bill bil);
	
	boolean modifySubTotal(Integer id, Bill bil);
	
	boolean modifyID(Integer id, Bill bil);
	
	boolean delete(Bill bil);
}
